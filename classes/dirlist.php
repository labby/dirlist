<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */
 
class dirlist extends LEPTON_abstract
{
	public array $settings = [];
	public string $addon_color = 'olive';
	public string $action_url = ADMIN_URL . '/pages/modify.php?page_id';
	public string $readme_link = 'https://cms-lab.com/_documentation/dirlist.php';
		
	public object|null $oTwig = null;
	public LEPTON_database $database;
	static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();	
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('dirlist');		
	}
	
	public function init_addon( $iSectionId = 0, $iPageId = 0 )
	{
		$this->action_url = ADMIN_URL . '/pages/modify.php?page_id='.$iPageId;
		
		//get settings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_dirlist WHERE section_id = ".$iSectionId,
			true,
			$this->settings,
			false
		);		
	}

	public function show_info() 
	{
		// create links	
		$support_link = "<a href='#'>NO Live-Support / FAQ</a>";
		$readme_link_ext = "<a href='".$this->readme_link."' class='info' target='_blank'>Readme</a>";		

		// data for twig template engine	
		$data = array(
			'oDL'			=> $this,
			'readme_link'	=> $readme_link_ext,		
			'SUPPORT'		=> $support_link,
			'leptoken'		=> get_leptoken(),			
			'image_url'		=> 'https://cms-lab.com/_documentation/media/dirlist/dirlist.jpg'
			);

		// get the template-engine.			
		echo $oTwig->render( 
			"@dirlist/info.lte",	//	template-filename
			$data					//	template-data
		);		
		
	}
	
	public function save_settings()
	{	
			$oREQUEST = LEPTON_request::getInstance();
			
			$all_names = array (
				'page_id'		=> array ('type' => 'integer', 'default' => -1),
				'section_id'	=> array ('type' => 'integer', 'default' => -1),
				'header'		=> array ('type' => 'string_clean', 'default' => ""),
				'prefix'		=> array ('type' => 'string_clean', 'default' => ""),
				'directory'		=> array ('type' => 'string_clean', 'default' => ""),
				'sort'			=> array ('type' => 'integer', 'default' => -1),
				'exclude' 		=> array ('type' => 'integer', 'default' => -1),
				'blank' 		=> array ('type' => 'integer', 'default' => 1),
				'extensions'	=> array ('type' => 'string_clean', 'default' => ""),
				'suffix'		=> array ('type' => 'string_clean', 'default' => "")
			);
			
			$all_values = $oREQUEST->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_dirlist";		
			$this->database->build_and_execute( 
				'UPDATE', 
				$table, 
				$all_values, 
				'section_id ='.$_POST['section_id']
			);
			
			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['dl_backend_success_update'],'pre','ui green message'));
			header("Refresh:1; url=".$this->action_url."&leptoken=".$_POST['leptoken']." ");	
			die();			
	}
}
