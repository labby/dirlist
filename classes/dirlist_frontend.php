<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

class dirlist_frontend extends dirlist
{
	public string $sFeAction = '';
	public string $sFileUrl = '';
	public string $sIconUrl = LEPTON_URL.'/modules/dirlist/img/';
	public array $aSettings = [];
	public array $aAllFiles = [];
	
	public object|null $oDMT = null;
	public object|null $oTwig = null;
	public LEPTON_database $database;
	static $instance;
	
public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('dirlist');
		$this->oDMT = dirlist_mimetypes::getInstance();
		$this->sFeAction = LEPTON_URL.PAGES_DIRECTORY.LEPTON_frontend::getInstance()->page['link'].PAGE_EXTENSION;	
	}

public function init_addon( $iPageId = -1, $iSectionId = -1)
	{
		//get array of aSettings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_dirlist WHERE section_id=". $iSectionId."  ",
			true,
			$this->aSettings,
			false
		);
		
		$this->sFileUrl = LEPTON_URL.MEDIA_DIRECTORY.$this->aSettings['directory'].'/';
		
		//get all files of saved directory
		$sDirScan = LEPTON_PATH.MEDIA_DIRECTORY.$this->aSettings['directory'];
		$aTempFiles = array_slice(scanDir($sDirScan), 2);		
		$aIgnoreFiles = array(".htaccess", "index.php");
		$this->aAllFiles = array_diff($aTempFiles, $aIgnoreFiles);		
	}

public function display_list()
	{

		// exclude or include files
		if($this->aSettings['extensions'] != '')
		{
			$this->aSettings['extensions'] = str_replace(' ','',$this->aSettings['extensions']);
			$aTemp = explode(",", $this->aSettings['extensions']);
	
			// only display specified elements 
			if($this->aSettings['exclude'] == 0)
			{
				$aInclude = [];				
				foreach($this->aAllFiles as &$file)
				{
					$mime = explode('.',$file);
					$mime = array_pop($mime);

					if(!in_array($mime,$aTemp))
					{
						$aInclude[] = $file;
					}
				}				
				
				$this->aAllFiles = array_diff($this->aAllFiles, $aInclude);					
			}
			
			// don't display specified elements
			if($this->aSettings['exclude'] == 1)
			{
				$aExclude = [];
				foreach($this->aAllFiles as &$file)
				{
					$mime = explode('.',$file);
					$mime = array_pop($mime);

					if(in_array($mime,$aTemp))
					{
						$aExclude[] = $file;
					}
				}
				
				$this->aAllFiles = array_diff($this->aAllFiles, $aExclude);		
			}
		}	

		// get data for output view
		$sDirScan = LEPTON_PATH.MEDIA_DIRECTORY.$this->aSettings['directory'];	
		$aSelectedFiles = [];
        foreach ($this->aAllFiles as $files) 
		{
            $aSelectedFiles[] = [				
				'file_img' => $this->oDMT->getIconByType($sDirScan . '/' . $files),
                'file_name' => $files,
                'file_size' => $this->oDMT->formatFilesizeUnits(filesize($sDirScan . '/' . $files)),
				'file_byte_size' => filesize($sDirScan . '/' . $files),	// only for sort purposes
                'file_date' => date("d.m.y", filemtime($sDirScan . '/' . $files)),
                'file_type' => $this->oDMT->getMimeType($sDirScan . '/' . $files)
            ];
        }	
	
		// sort array
		switch ($this->aSettings['sort'])
		{		
			case 0:		// sortFilesDescending
				usort($aSelectedFiles, function($y, $x) 
					{
						return $x['file_name'] <=> $y['file_name'];
					});
				break;
			case 1:		// sortFilesAscending -> default
				usort($aSelectedFiles, function($x, $y) 
					{
						return $x['file_name'] <=> $y['file_name'];
					});
				break;			
			case 2:		// sortDateAscending
				usort($aSelectedFiles, function($x, $y) 
					{
						return $x['file_date'] <=> $y['file_date'];
					});
				break;				
			case 3:		// sortDateDescending
				usort($aSelectedFiles, function($y, $x) 
					{
						return $x['file_date'] <=> $y['file_date'];
					});
				break;				
			case 4:		// sortSizeAscending
				usort($aSelectedFiles, function($x, $y) 
					{
						return $x['file_byte_size'] <=> $y['file_byte_size'];
					});
				break;				
			case 5:		// sortSizeDescending
				usort($aSelectedFiles, function($y, $x) 
					{
						return $x['file_byte_size'] <=> $y['file_byte_size'];
					});
				break;				
			
			default:
				$aSelectedFiles = 'Error';
		}
		
	
		// data for twig template engine	
		$data = array(
			'oDLFE'				=> $this,
			'aSelectedFiles'	=> $aSelectedFiles

		);
		
		//	get the template-engine
		echo $this->oTwig->render( 
			"@dirlist/view.lte",	//	template-filename
			$data					//	template-data
		);				
	}		
}
