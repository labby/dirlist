<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

$files_to_register = array(
	'delete.php',
	'modify.php',
	'add.php'
);
LEPTON_secure::getInstance()->accessFiles( $files_to_register );
