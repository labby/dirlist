<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php



// get instance of own module class
$oDL = dirlist::getInstance();
$oDL->init_addon( $section_id, $page_id );

// call methods
if(isset($_POST['job']) && $_POST['job'] == 'save_settings' ) 
{
	$oDL->save_settings();
	die();
}

if(isset($_POST['show_info']) && $_POST['show_info'] == 'show' ) 
{
	$oDL->show_info();
	die();
}

// collect some data
LEPTON_handle::register( "directory_list" );
$aEmptyList = [];
$skipPathKomponent = LEPTON_PATH.MEDIA_DIRECTORY;
$directories = directory_list( LEPTON_PATH.MEDIA_DIRECTORY, ignore:$skipPathKomponent);

//echo (LEPTON_tools::display($directories,'pre','ui message'));

$form_values = array(
	'oDL'			=> $oDL,
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'directories'	=> $directories,
	'leptoken'		=> get_leptoken()	
);

/**	
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('dirlist');
	
echo $oTwig->render("@dirlist/modify.lte", $form_values);	
