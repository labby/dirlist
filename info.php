<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

//  L* 5 backward compatibility for old modules
if (class_exists("lib_comp", true))
{
    lib_comp::init("dirlist");
}

$module_directory   = 'dirlist';
$module_name        = 'DirList';
$module_function    = 'page';
$module_version     = '1.1.1';
$module_platform    = '7.x';
$module_status      = 'Stable';
$module_author      = 'cms-lab';
$module_license     = 'MIT License (MIT)';
$module_description = 'Show files of a selected MEDIA directory with mime-type icon, name, size and date of last change for download.';
$module_home        = 'https://cms-lab.com';
$module_guid        = 'DC9E87CA-0D9E-47E0-9CF1-9CBFF6EFD31C';

/**
 *  changelog:
 *  https://github.com/labby/dirlist
 *
 */
