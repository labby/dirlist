<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

	

// upgrade < 0.9.0
$delete_dir = array(
	'/modules/dirlist/htt'
);
LEPTON_handle::delete_obsolete_directories( $delete_dir );

$to_delete = array(
	'/modules/dirlist/backend.css',
	'/modules/dirlist/frontend.css',
	'/modules/dirlist/class.dirlist.php',
	'/modules/dirlist/class.parser.php',
	'/modules/dirlist/class.mimetypes.php',
	'/modules/dirlist/languages/FR.php',
	'/modules/dirlist/languages/IT.php',
	'/modules/dirlist/languages/NL.php',
	'/modules/dirlist/save.php',
	'/modules/dirlist/classes/dirlist_parser.php',
	'/modules/dirlist/classes/dirlist_extended.php'
);
LEPTON_handle::delete_obsolete_files( $to_delete );

$database = LEPTON_database::getInstance();
$database->simple_query("ALTER TABLE ".TABLE_PREFIX."mod_dirlist DROP `last_modified` ");
$database->simple_query("ALTER TABLE ".TABLE_PREFIX."mod_dirlist ADD `last_modified` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `blank` ");

// modify directory value
//get settings
$aSettings = [];
$database->execute_query(
    "SELECT * FROM ".TABLE_PREFIX."mod_dirlist ",
    true,
    $aSettings,
    true
);

foreach ($aSettings as $temp)
{
    if (substr($temp['directory'], -1) == "/")
    {
        $temp['directory'] = substr($temp['directory'], 0, -1);
        $database->simple_query("UPDATE `".TABLE_PREFIX."mod_dirlist` SET `directory` = '".$temp['directory']."' WHERE `section_id` = ".$temp['section_id']);	
    }
}
