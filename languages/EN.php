<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$MOD_DIRLIST = array(
    'dl_error_add_record' 		=> '<p>a error occurs while adding a new record</p><p>[%s] <strong>%s</strong></p>',
    'dl_error_create_table' 	=> '<p>a error occurs while trying to create a new table</p><p>[%s] <strong>%s</strong></p>',
    'dl_error_delete_record' 	=> '<p>a error occurs while deleting a record</p><p>[%s] <strong>%s</strong></p>',
    'dl_error_delete_table' 	=> '<p>a error occurs while deleting a table</p><p>[%s] <strong>%s</strong></p>',
    'dl_error_describe_table' 	=> '<p>Could not read the table description.</p><p><strong>Prompt:</strong> %s</p>',
    'dl_error_empty_directory' 	=> 'Please select a MEDIA directory!',
    'dl_error_header' 			=> 'dirlist.php',
    'dl_error_insert_field' 	=> '<p>A error occurs while inserting the field "<strong>%s</strong>" into the table.</p><p><strong>Prompt:</strong> %s</p>',
    'dl_error_link_by_page_id' 	=> 'The filename of the page could not be read from the database.',
    'dl_error_no_error' 		=> 'no error',
    'dl_error_not_specified' 	=> 'not specified',
    'dl_error_row_empty' 		=> 'no entry in table',
    'dl_error_update_record' 	=> '<p>a error occurs while updating the record</p><p>[%s] <strong>%s</strong></p>',

    'dl_upgrade_field_exists' 	=> '<p>The field "<strong>%s</strong>" is still existing.</p>',
    'dl_upgrade_insert_field' 	=> '<p>The field "<strong>%s</strong>" was successfully inserted into the table.</p>',

    'dl_backend_blank' 			=> 'Open files in a new window',
    'dl_backend_btn_abort' 		=> 'Abort',
    'dl_backend_btn_submit' 	=> 'Submit',
    'dl_backend_description' 	=> 'With this dialog you configure DirList for the selected page rsp. section',
    'dl_backend_exclude' 		=> 'Exclude',
    'dl_backend_header' 		=> 'Configure DirList',
    'dl_backend_include' 		=> 'Include',
    'dl_backend_select_directory' 	=> '--> Please select a directory!',
    'dl_backend_sortDateAscending' 	=> 'by date, ascending',
    'dl_backend_sortDateDescending' => 'by date, descending',
    'dl_backend_sortFilesAscending' => 'by filename, ascending',
    'dl_backend_sortFilesDescending'=> 'by filename, descending',
    'dl_backend_sortSizeAscending' 	=> 'by filesize, ascending',
    'dl_backend_sortSizeDescending' => 'by filesize, descending',
    'dl_backend_success_update' => 'The record was successfully updated',
    'dl_backend_text_header' 	=> 'Header for the DirList',
    'dl_backend_text_preselect' => 'You may include and exclude files. Please type in the file extensions separated by comma, i.e.: <strong>tif, jpg, gif</strong>',
    'dl_backend_text_prefix' 	=> 'This text will be shown after the header and before the DirList (Prefix)',
    'dl_backend_text_select' 	=> 'Directory used by DirList',
    'dl_backend_text_sort' 		=> 'Sorting the DirList:',
    'dl_backend_text_suffix' 	=> 'This text will be shown at the end of page rsp. section (Suffix)',

    'dl_frontend_empty_directory' => '..',
    'dl_frontend_th_date' 		=> 'Modified',
    'dl_frontend_th_datetime' 	=> 'm-d-Y g:i a',
    'dl_frontend_th_file' 		=> 'Filename',
	'dl_frontend_th_mime'		=>	'Media Type',
    'dl_frontend_th_size' 		=> 'Size'
);