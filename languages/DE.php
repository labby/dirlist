<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$MOD_DIRLIST = array(
	'dl_error_add_record'		=>	'<p>Beim Hinzufügen des Datensatz ist ein Fehler aufgetreten</p><p>[%s] <strong>%s</strong></p>',
	'dl_error_create_table'		=>	'<p>Beim Anlegen der Tabelle für DirList ist ein Fehler aufgetreten</p><p>[%s] <strong>%s</strong></p>',
	'dl_error_delete_record'	=>	'<p>Beim Löschen des Datensatz ist ein Fehler aufgetreten</p><p>[%s] <strong>%s</strong></p>',
	'dl_error_delete_table'		=>	'<p>Beim Löschen der Tabelle für DirList ist ein Fehler aufgetreten</p><p>[%s] <strong>%s</strong></p>',
	'dl_error_describe_table'	=>	'<p>Die Tabellenbeschreibung konnte nicht ausgelesen werden.</p><p><strong>Fehlermeldung:</strong> %s</p>',
	'dl_error_empty_directory'	=> 	'Bitte geben Sie ein MEDIA Verzeichnis an!',
	'dl_error_header'			=>	'dirlist.php',
	'dl_error_insert_field'		=>	'<p>Beim Einfügen des Feldes "<strong>%s</strong>" ist ein Fehler aufgetreten.</p><p><strong>Fehlermeldung:</strong> %s</p>',
	'dl_error_link_by_page_id'	=>	'Der Dateiname der Seite konnte nicht ausgelesen werden.',
	'dl_error_no_error'			=>	'kein Fehler',
	'dl_error_not_specified'	=>  'nicht spezifiziert',
	'dl_error_row_empty'		=>	'kein Eintrag in der Tabelle',
	'dl_error_update_record'	=>	'<p>Beim Aktualisieren des Datensatz ist ein Fehler aufgetreten</p><p>[%s] <strong>%s</strong></p>',

	'dl_upgrade_field_exists'	=>	'<p>Das Feld "<strong>%s</strong>" existiert bereits.</p>',
	'dl_upgrade_insert_field'	=>	'<p>Das Feld "<strong>%s</strong>" wurde in die Tabelle eingefügt.</p>',

	'dl_backend_blank'			=>	'Dateien in einem neuen Fenster öffnen',
	'dl_backend_btn_abort'		=>	'Abbrechen',
	'dl_backend_btn_submit'		=>	'übernehmen',
	'dl_backend_description'	=>  'Mit diesem Dialog richten Sie DirList für die jeweilige Seite bzw. für den jeweiligen Abschnitt ein.',
	'dl_backend_exclude'		=>	'Ausschließen',
	'dl_backend_header'			=>  'DirList konfigurieren',
	'dl_backend_include'		=>	'Einschließen',
	'dl_backend_select_directory'	=>	'--> Bitte wählen Sie ein Verzeichnis aus!',
	'dl_backend_sortDateAscending'	=>	'nach Datum, aufsteigend',
	'dl_backend_sortDateDescending'	=>	'nach Datum, absteigend',
	'dl_backend_sortFilesAscending'	=>	'nach Dateinamen, aufsteigend',
	'dl_backend_sortFilesDescending'=>	'nach Dateinamen, absteigend',
	'dl_backend_sortSizeAscending'	=>	'nach Dateigröße, aufsteigend',
	'dl_backend_sortSizeDescending'	=>	'nach Dateigröße, absteigend',
	'dl_backend_success_update'	=>	'Der Datensatz wurde aktualisiert',
	'dl_backend_text_header'	=>  'Überschrift der DirList',
	'dl_backend_text_preselect'	=>	'<p>Sie können festlegen, ob bestimmte Dateien <strong>ein</strong>- oder <strong>aus</strong>geschlossen werden sollen.<br />Geben Sie die gewünschten Dateiendungen durch ein Komma getrennt ein, z.B: <strong>tif, gif, jpg</strong></p>',
	'dl_backend_text_prefix'	=>  'Erläuterungen zur Funktion der DirList (Prefix)',
	'dl_backend_text_select'	=>	'MEDIA Verzeichnis der DirList',
	'dl_backend_text_sort'		=>	'Sortierung der DirList:',
	'dl_backend_text_suffix'	=>	'Dieser Text wird nach der DirList auf der Seite angezeigt (Suffix)',

	'dl_frontend_empty_directory'	=>	'..',
	'dl_frontend_th_date'		=>	'Geändert',
	'dl_frontend_th_datetime'	=>	'd.m.Y h:i',
	'dl_frontend_th_file'		=>	'Dateiname',
	'dl_frontend_th_mime'		=>	'Medien-Typ',
	'dl_frontend_th_size'		=>	'Größe'
);