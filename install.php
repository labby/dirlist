<?php

/**
 *  @module         DirList, first released by Ralf Hertsch (†)
 *  @version        see info.php of this module
 *  @authors        cms-lab
 *  @copyright      2022 - 2024 cms-lab
 *  @link           https://cms-lab.com 
 *  @license        MIT License (MIT) http://www.opensource.org/licenses/MIT
 *  @license terms  see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// Create the table
$table_fields="
	  `section_id` INT(11) NOT NULL DEFAULT 0,
	  `page_id` INT(11) NOT NULl DEFAULT 0,
	  `directory` VARCHAR(255) NOT NULL DEFAULT '',
	  `sort` INT(11) NOT NULL DEFAULT 1,
	  `header` VARCHAR(255) NOT NULL DEFAULT '',
	  `prefix` TEXT,
	  `suffix` TEXT,
	  `exclude` INT(11) NOT NULL DEFAULT '1',
	  `extensions` VARCHAR(255) NOT NULL DEFAULT '',
	  `blank` INT(11) NOT NULL DEFAULT 1,
	  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY ( section_id )
	";
LEPTON_handle::install_table("mod_dirlist", $table_fields);
